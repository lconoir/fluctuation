from random import randint
from math import sqrt
from matplotlib import pyplot


# n expériences de tirage d'un dé : cas où on n'obtient pas 6
def tirage(n) :
    compteur = 0
    for i in range(n) :
        if randint(1,6) != 6 :
            compteur = compteur + 1
    return compteur


# liste des fréquences de n tirages, pour N échantillons
def echantillons(n,N) :
    liste = []
    for i in range(1, N + 1) :
        liste.append(tirage(n)/n)
    return liste


# nombre d'échantillon dans l'intervalle
def nb_echantillons_inclus(liste,n) :
    compteur = 0
    for valeur in liste :
        if abs(valeur - 5/6) < 1/sqrt(n) :
            compteur = compteur + 1
    return compteur


# affichage des points
def graphique(n,N,nb) :
    pyplot.figure(nb)

    # fenêtre d'affichage
    pyplot.axis(xmin=0, xmax=N, ymin=0, ymax=1)
    pyplot.title(str(N) + ' échantillons de taille ' + str(n))
    pyplot.xlabel("Echantillons")
    pyplot.ylabel('Fréquences')
    
        
    # affichage de l'intervalle de fluctuation  [p - 1/sqrt(n) ; p - 1/sqrt(n)]
    pyplot.plot([0, N] , [5/6, 5/6], 'r--')
    pyplot.plot([0, N] , [5/6 + 1/sqrt(n), 5/6 + 1/sqrt(n)],'g-')
    pyplot.plot([0, N] , [5/6 - 1/sqrt(n), 5/6 - 1/sqrt(n)],'g-')
    
    # affichage des fréquences
    liste_x = []
    for x in range(1, N + 1) :
        liste_x.append(x)
    liste_y = echantillons(n,N)
    pyplot.plot(liste_x, liste_y, 'b.')
    
    # décompte des échantillons dans l'intervalle
    valides = nb_echantillons_inclus(liste_y,n)
    texte = 'Echantillons valides : {0} ( {1:3.1f} %)'.format(valides,valides/N*100)
    pyplot.text(4,5/6 - 1/sqrt(n) - 0.5,texte)


# variation du nombre d'échantillons : [N_min ; N_max]
def variation_echantillon(n, N_min, N_max, ecart) :
    nb_graphique = 1
    for nombre_N in range(N_min,N_max+1,ecart) :
        graphique(n, nombre_N, nb_graphique)
        nb_graphique = nb_graphique + 1
    pyplot.show()

# variation du nombre de la taille de l'échantillon : [n_min ; n_max]
def variation_tirage(n_min, n_max, N, ecart) :
    nb_graphique = 1
    for nombre_n in range(n_min,n_max+1,ecart) :
        graphique(nombre_n, N, nb_graphique)
        nb_graphique = nb_graphique + 1
    pyplot.show()


# appels des fonctions
#variation_echantillon(50,20,100,20)       
variation_tirage(50,200,100,50)           
